﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Hackatown2018.Models;
using MongoDB.Bson;

namespace Hackatown2018.Controllers
{
    public class ValuesController : ApiController
    {

        DataAccess dataAccess = new DataAccess();

        [Route("api/Watchdog/u")]
        [HttpGet]
        public IEnumerable<User> GetUsers()
        {
            List<User> users = new List<User>();
            List<Userdb> dbUsers = dataAccess.GetUsers();
            foreach (Userdb udb in dbUsers)
                users.Add(new User(udb));

            return users;
        }

        [Route("api/Watchdog/s")]
        [HttpGet]
        public IEnumerable<string> GetSubjects()
        {
            return dataAccess.GetSubjects();
        }

        [Route("api/Watchdog/u/{id}")]
        public User GetUser(string id)
        {
            try
            {
                User u = new User(dataAccess.GetUser(ObjectId.Parse(id)));
                return u;
            }
            catch (FormatException E)
            {
                return new User(dataAccess.GetUser(ObjectId.Parse(MatchingController.hash(id))));
            }

        }

        [Route("api/Watchdog/m/{id}")]
        [HttpGet]
        public IEnumerable<User> getUsersFromTag(string id)
        {
            try
            {
                return MatchingController.FindMatch(dataAccess.GetUser(ObjectId.Parse(id)), dataAccess);
            }
            catch (FormatException E)
            {
                return MatchingController.FindMatch(dataAccess.GetUser(ObjectId.Parse(MatchingController.hash(id))), dataAccess);
            }
        }

        [Route("api/Watchdog/u")]
        [HttpPost]
        public void PostUser([FromBody]User value)
        {
            Userdb udb;
            if (value.ID == "")
                value.ID = ObjectId.GenerateNewId().ToString();
            try
            {
                udb = new Userdb(value);
            }
            catch (FormatException E)
            {
                value.ID = MatchingController.hash(value.ID);
                udb = new Userdb(value);
            }

            dataAccess.AddUser(udb);
        }

        [Route("api/Watchdog/s")]
        [HttpPost]
        public void PostSubject([FromBody]string value)
        {
            dataAccess.AddSubject(value);
        }

        [Route("api/Watchdog/u/{id}")]
        [HttpPut]
        public void PutUser(string id, [FromBody]User value)
        {
            try
            {
                dataAccess.UpdateUser(ObjectId.Parse(id), new Userdb(value));
            }
            catch (FormatException E)
            {
                id = MatchingController.hash(id);
                value.ID = id;
                dataAccess.UpdateUser(ObjectId.Parse(id), new Userdb(value));
            }
        }

        [Route("api/Watchdog/u/i/{id}")]
        [HttpPut]
        public void IncScore(string id)
        {
            dataAccess.incrementScore(ObjectId.Parse(id));
        }

        [Route("api/Watchdog/u/d/{id}")]
        [HttpPut]
        public void DecScore(string id)
        {
            dataAccess.decrementScore(ObjectId.Parse(id));
        }


        [Route("api/Watchdog/u/{id}")]
        [HttpDelete]
        public void DeleteUser(string id)
        {
            try
            {
            dataAccess.RemoveUser(ObjectId.Parse(id));
            }
            catch(FormatException e)
            {
                dataAccess.RemoveUser(ObjectId.Parse(MatchingController.hash(id)));
            }
        }

        [Route("api/Watchdog/s/{id}")]
        [HttpDelete]
        public void DeleteSubject(string id)
        {
            // TODO: GODU
            dataAccess.RemoveSubject(id);
        }

    }
}
