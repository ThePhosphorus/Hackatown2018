﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hackatown2018.Models;

namespace Hackatown2018.Controllers
{
    public class MatchingController
    {
        const float MIN_MATCH = 0.0f;

        static public string hash( string str)
        {
            string endResult = "";
            for (int i = 0; i < 12; i++)
                endResult += ((int)str[i]).ToString("x");
            return endResult;
        }

        static public IList<User> FindMatch(BaseUser user, DataAccess da)
        {
            List<Userdb> usersDB = da.GetUsers();

            SortedList<float,User> ret = new SortedList<float,User>();

            foreach (Userdb u in usersDB)
            {
                float matchFactor = 0;
                if (isMatch(user, u, ref matchFactor))
                {
                    try
                    {
                        ret.Add(matchFactor, new User(u));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                }
            }

            return ret.Values;
        }

        static private bool isMatch(BaseUser original, BaseUser compared, ref float matchFactor)
        {
            matchFactor = isPartialMatch(original, compared, true) + isPartialMatch(compared, original, false);
            matchFactor /= 2;
            compared.Match = matchFactor;
            return (matchFactor > MIN_MATCH);
        }

        static private float isPartialMatch(BaseUser receiver, BaseUser giver, bool receiverIsOriginal)
        {
            uint numberOfhits = 0;
            HashSet<string> subjectHit = new HashSet<string>();
            foreach (string str1 in receiver.Interests)
                foreach (string str2 in giver.Knowledges)
                    if (str1 == str2)
                    {
                        numberOfhits++;
                        subjectHit.Add(str1);
                    }

            if (receiverIsOriginal)
                giver.Knowledges = subjectHit;
            else
                receiver.Interests = subjectHit;

            return (numberOfhits / ((receiverIsOriginal)? receiver.Interests.Count : giver.Knowledges.Count));
        }
    }
}