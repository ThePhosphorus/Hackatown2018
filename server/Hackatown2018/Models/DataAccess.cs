﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace Hackatown2018.Models
{
    public class DataAccess
    {
        MongoClient _client;
        IMongoDatabase _db;

        public DataAccess()
        {
            _client = new MongoClient("mongodb://help:h@ds042607.mlab.com:42607/geniade_web-test_server");
            _db = _client.GetDatabase("geniade_web-test_server");
        }

        public List<Userdb> GetUsers()
        {
            return _db.GetCollection<Userdb>("Hackatown2018").Find(b => true).ToList();
        }

        public List<string> GetSubjects()
        {
            return _db.GetCollection<string>("Hackatown2018").Find(b => true).ToListAsync().Result;

        }

        public Userdb GetUser(ObjectId id)
        {
            var res = Builders<Userdb>.Filter.Eq(u => u.ID, id);
            return _db.GetCollection<Userdb>("Hackatown2018").Find(res).FirstOrDefault();
        }

        public void AddUser(Userdb user)
        {
            _db.GetCollection<Userdb>("Hackatown2018").InsertOne(user);
        }

        public void AddSubject(string subject)
        {
            _db.GetCollection<string>("Hackatown2018").InsertOne(subject);
        }

        public void UpdateUser(ObjectId id, Userdb user)
        {
            var res = Builders<Userdb>.Filter.Eq(u => u.ID, id);
            _db.GetCollection<Userdb>("Hackatown2018").ReplaceOne(res, user);
        }

        public void RemoveUser(ObjectId id)
        {
            var res = Builders<Userdb>.Filter.Eq(e => e.ID, id);
            var operation = _db.GetCollection<Userdb>("Hackatown2018").DeleteOne(res);
        }

        public void RemoveSubject(string id)
        {
            var res = Builders<string>.Filter.Eq(e => e, id);
            var operation = _db.GetCollection<string>("Hackatown2018").DeleteOne(res);
        }

        public void incrementScore (ObjectId id)
        {
            var res = Builders<Userdb>.Filter.Eq(u => u.ID, id);
            _db.GetCollection<Userdb>("Hackatown2018").UpdateOne(res, 
                Builders<Userdb>.Update.Inc("score",1));
        }

        public void decrementScore(ObjectId id)
        {
            var res = Builders<Userdb>.Filter.Eq(u => u.ID, id);
            _db.GetCollection<Userdb>("Hackatown2018").UpdateOne(res,
                Builders<Userdb>.Update.Inc("score", -1));
        }
    }
}