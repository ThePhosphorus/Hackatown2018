﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace Hackatown2018.Models
{
    public class BaseUser
    {
        protected string _userName;
        string _email;
        string _bio;
        float _match;
        int _score;

        ISet<string> _interests;
        ISet<string> _knowledges;
        public BaseUser()
        {
            _interests = new HashSet<string>();
            _knowledges = new HashSet<string>();
        }
        public BaseUser(BaseUser u)
        {
            if (u != null)
            {
                _userName = u._userName;
                _email = u._email;
                _bio = u._bio;
                _match = u._match;
                _score = u._score;
                _knowledges = u.Knowledges;
                _interests = u.Interests;
            }
        }


        [BsonIgnore]
        public float Match
        {
            get { return _match; }
            set { _match = value; }
        }

        [BsonElement("bio")]
        public string Bio
        {
            get { return _bio; }
            set { _bio = value; }
        }


        [BsonElement("score")]
        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }



        [BsonElement("username")]
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        [BsonElement("email")]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        [BsonElement("interests")]
        public ISet<string> Interests
        {
            get { return _interests; }
            set { _interests = value; }
        }

        public bool isInterested(string id)
        {
            return _interests.Contains(id);
        }

        public void addInterest(string id)
        {
            _interests.Add(id);
        }

        public void deleteInterest(string id)
        {
            _interests.Remove(id);
        }

        [BsonElement("knowledges")]
        public ISet<string> Knowledges
        {
            get { return _knowledges; }
            set { _knowledges = value; }
        }

        public bool isKnown(string id)
        {
            return _knowledges.Contains(id);
        }

        public void addKnowledge(string id)
        {
            _knowledges.Add(id);
        }

        public void deleteKnowledge(string id)
        {
            _knowledges.Remove(id);
        }
    }
    public class User : BaseUser
    {
        string _strId;


        public string ID
        {
            get { return _strId; }
            set { _strId = value; }
        }

        public User() : base()
        {

        }
        public User(Userdb u) : base(u)
        {
            if (u != null)
                _strId = u.ID.ToString();
        }
    }

    public class Userdb : BaseUser
    {
        ObjectId _id;

        [BsonId]
        public ObjectId ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public Userdb() : base()
        {
        }
        public Userdb(User u) : base(u)
        {
            if (u != null)
                _id = ObjectId.Parse(u.ID);
        }
    }
}