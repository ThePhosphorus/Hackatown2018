﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Hackatown2018.Models
{
    public class Subject
    {
        string _id;

        string _name;

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public Subject() { }
        public Subject(Subjectdb s)
        {
            _name = s.Name;
            _id = s.ID.ToString();
        }
    };

    public class Subjectdb
    {
        ObjectId _id;

        string _name;

        [BsonId]
        public ObjectId ID
        {
            get { return _id; }
            set { _id = value; }
        }

        [BsonElement("name")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public Subjectdb() { }
        public Subjectdb(Subject s)
        {
            _name = s.Name;
            _id = ObjectId.Parse(s.ID);
        }
    };
}