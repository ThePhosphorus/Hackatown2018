package com.beast.hackatown.hackatown;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Xml;
import android.widget.TextView;
import java.util.Random;

import com.android.volley.toolbox.JsonRequest;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONObject;


public class ViewProfileActivity extends AppCompatActivity {
    Communication communication;
    String profileId;
    int match;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);

        Intent intent = getIntent();
        profileId = intent.getStringExtra("profileId");

        match = intent.getIntExtra("match",0);

        communication = new Communication(this);
        communication.getProfileInfo(profileId, new Communication.responseCallbackInterface() {
            public void callbackOnSuccess(User[] users) {}
            @Override
            public void callbackOnSuccess(User user) {
                user.Match = match;
                showProfile(user);
            }

            @Override
            public void callbackOnFailure() {
                showErrorMessage();
            }
        });
    }

    public void showProfile(User user) {
        TextView nameView = findViewById(R.id.Name_value);
        nameView.setText(user.UserName);

        TextView distView = findViewById(R.id.Distance_Value);
        Integer dist = new Random().nextInt(250);
        String distString = dist.toString() + " km";
        distView.setText(distString);

        TextView matchView =findViewById(R.id.Match_Value);
        if(!user.ID.equals( FirebaseAuth.getInstance().getUid()) ){
            String matchString = user.Match +"%";
            //matchView.setText(user.Match+'%');
            matchView.setText(matchString);
        }else
        {
            matchView.setText("--%");
        }


        TextView scoreView =findViewById(R.id.Helpful_value);
        String score = user.Score +"%";
        scoreView.setText(score);

        TextView bioView =findViewById(R.id.Bio_Value);
        bioView.setText(user.Bio);

        String interestString = "";
        for (String interest:user.Interests
             ) {
            interestString+=interest + " , ";

        }
        interestString=interestString.substring(0,interestString.length()-2);

        TextView intView = findViewById(R.id.Int_Value);
        intView.setText(interestString);

        String knowledgeString = "";

        for (String interest:user.Knowledges
                ) {
            knowledgeString += interest+" , ";

        }
        knowledgeString=knowledgeString.substring(0,knowledgeString.length()-2);

        TextView knoView =findViewById(R.id.Know_Value);
        knoView.setText(knowledgeString);



    }

    public void showErrorMessage() {

    }

}
