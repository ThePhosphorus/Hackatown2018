package com.beast.hackatown.hackatown;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;

public class EditProfileActivity extends AppCompatActivity {
    private Communication communication;
    String profileId;
    User currentUser;
    private Button addInterest, addKnowledge;
    private TextView nom, bio, interests, knowledges;
    private LinearLayout interestLayout, knowledgeLayout, nameLayout, bioLayout;
    public static boolean isNewUser = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        Intent intent = getIntent();
        profileId = intent.getStringExtra("profileId");

        communication = new Communication(this);
        communication.getProfileInfo(profileId, new Communication.responseCallbackInterface() {
            public void callbackOnSuccess(User[] users) {}
            @Override
            public void callbackOnSuccess(User user) {
                showProfile(user);
            }

            @Override
            public void callbackOnFailure() {
                showErrorMessage();
            }
        });

        interestLayout = new LinearLayout(this);
        knowledgeLayout = new LinearLayout(this);
        nameLayout = new LinearLayout(this);
        bioLayout = new LinearLayout(this);
        interestLayout.setOrientation(LinearLayout.VERTICAL);
        knowledgeLayout.setOrientation(LinearLayout.VERTICAL);
        nameLayout.setOrientation(LinearLayout.HORIZONTAL);
        bioLayout.setOrientation(LinearLayout.HORIZONTAL);
        TextView nameText = new TextView(this);
        nameText.setText("Name: ");
        nameLayout.addView(nameText);
        nom = new EditText(this);
        nom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        nameLayout.addView(nom);
        TextView bioText = new TextView(this);
        bioText.setText("Bio: ");
        bioLayout.addView(bioText);
        bio = new EditText(this);
        bio.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        bioLayout.addView(bio);
        interests = new TextView(this);
        knowledges = new TextView(this);
        addInterest = new Button(this);
        addKnowledge = new Button(this);
        interests.setText("Interests");
        knowledges.setText("Knowledge");
        addInterest.setText("Add interest");
        addInterest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> array = new ArrayList<>();
                for(int i=0; i<interestLayout.getChildCount(); i++) {
                    if(interestLayout.getChildAt(i) instanceof EditText ) {
                        if(((EditText)interestLayout.getChildAt(i)).getText().toString() != "")
                            array.add(((EditText)interestLayout.getChildAt(i)).getText().toString());
                    }
                }
                array.add("");
                currentUser.Interests = (String[]) array.toArray(new String[array.size()]);
                array = new ArrayList<>();
                for(int i=0; i<knowledgeLayout.getChildCount(); i++) {
                    if(knowledgeLayout.getChildAt(i) instanceof EditText ) {
                        if(((EditText)knowledgeLayout.getChildAt(i)).getText().toString() != "")
                            array.add(((EditText)knowledgeLayout.getChildAt(i)).getText().toString());
                    }
                }
                currentUser.Knowledges = (String[]) array.toArray(new String[array.size()]);
                currentUser.UserName = ((EditText)nameLayout.getChildAt(1)).getText().toString();
                currentUser.Bio = ((EditText)bioLayout.getChildAt(1)).getText().toString();
                updateView();
            }
        });
        addKnowledge.setText("Add knowledge");
        addKnowledge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> array = new ArrayList<>();
                for(int i=0; i<knowledgeLayout.getChildCount(); i++) {
                    if(knowledgeLayout.getChildAt(i) instanceof EditText ) {
                        if(((EditText)knowledgeLayout.getChildAt(i)).getText().toString() != "")
                            array.add(((EditText)knowledgeLayout.getChildAt(i)).getText().toString());
                    }
                }
                array.add("");
                currentUser.Knowledges = (String[]) array.toArray(new String[array.size()]);
                array = new ArrayList<>();
                for(int i=0; i<interestLayout.getChildCount(); i++) {
                    if(interestLayout.getChildAt(i) instanceof EditText ) {
                        if(((EditText)interestLayout.getChildAt(i)).getText().toString() != "")
                            array.add(((EditText)interestLayout.getChildAt(i)).getText().toString());
                    }
                }
                currentUser.Interests = (String[]) array.toArray(new String[array.size()]);
                currentUser.UserName = ((EditText)nameLayout.getChildAt(1)).getText().toString();
                currentUser.Bio = ((EditText)bioLayout.getChildAt(1)).getText().toString();
                updateView();
            }
        });

        Button applyButton = findViewById(R.id.button_apply);
        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> array = new ArrayList<>();
                for(int i=0; i<interestLayout.getChildCount(); i++) {
                    if(interestLayout.getChildAt(i) instanceof EditText ) {
                        if(((EditText)interestLayout.getChildAt(i)).getText().toString() != "")
                            array.add(((EditText)interestLayout.getChildAt(i)).getText().toString());
                    }
                }
                currentUser.Interests = (String[]) array.toArray(new String[array.size()]);
                array = new ArrayList<>();
                for(int i=0; i<knowledgeLayout.getChildCount(); i++) {
                    if(knowledgeLayout.getChildAt(i) instanceof EditText ) {
                        if(((EditText)knowledgeLayout.getChildAt(i)).getText().toString() != "")
                            array.add(((EditText)knowledgeLayout.getChildAt(i)).getText().toString());
                    }
                }
                currentUser.Knowledges = (String[]) array.toArray(new String[array.size()]);
                currentUser.UserName = nom.getText().toString();
                currentUser.Bio = bio.getText().toString();
                communication.updateProfileInfo(currentUser, new Communication.responseCallbackInterface() {
                    public void callbackOnSuccess(User[] users) {}
                    @Override
                    public void callbackOnSuccess(User user) {
                        Toast toast = Toast.makeText(getApplicationContext(), "Success updating profile!", Toast.LENGTH_SHORT);
                        toast.show();
                    }

                    @Override
                    public void callbackOnFailure() {
                        Toast toast = Toast.makeText(getApplicationContext(), "Failure updating profile", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                });
            }
        });
        Button cancelButton = findViewById(R.id.button_cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        isNewUser = false;
    }

    public void showProfile(User user) {
        currentUser = user;
        currentUser.ID = profileId;
        if(currentUser.UserName == "")
            isNewUser = true;
        updateView();
    }

    public void showErrorMessage() {
        LinearLayout layout = findViewById(R.id.edit_profile_layout);
        TextView erreur = new TextView(this);
        layout.addView(erreur);
    }

    private void updateView() {
        LinearLayout layout = findViewById(R.id.edit_profile_layout);
        layout.removeAllViews();
        interestLayout.removeAllViews();
        knowledgeLayout.removeAllViews();
        nom.setText(currentUser.UserName);
        bio.setText(currentUser.Bio);
        interestLayout.addView(interests);
        for(String item: currentUser.Interests) {
            EditText edit = new EditText(this);
            edit.setText(item);
            interestLayout.addView(edit);
        }
        interestLayout.addView(addInterest);
        knowledgeLayout.addView(knowledges);
        for(String item: currentUser.Knowledges) {
            EditText edit = new EditText(this);
            edit.setText(item);
            knowledgeLayout.addView(edit);
        }
        knowledgeLayout.addView(addKnowledge);
        nameLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        bioLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        interestLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        knowledgeLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        layout.addView(nameLayout);
        layout.addView(bioLayout);
        layout.addView(interestLayout);
        layout.addView(knowledgeLayout);
    }
}
