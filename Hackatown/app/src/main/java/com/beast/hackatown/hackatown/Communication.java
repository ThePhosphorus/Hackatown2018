package com.beast.hackatown.hackatown;

    import android.app.DownloadManager;
    import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
    import com.android.volley.toolbox.JsonObjectRequest;
    import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

    import org.json.JSONArray;
    import org.json.JSONException;
    import org.json.JSONObject;

    import java.io.UnsupportedEncodingException;
    import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;

/**
 * Created by colin on 1/20/18.
 */

public class Communication {
    public interface responseCallbackInterface {
        void callbackOnSuccess(User user);
        void callbackOnSuccess(User[] users);
        void callbackOnFailure();
    }

    RequestQueue requestQueue;
    String baseURL;

    public Communication(Context context) {
        requestQueue = Volley.newRequestQueue(context);
        baseURL = "http://hackatown2018-dev.us-east-2.elasticbeanstalk.com/api/Watchdog/";
    }

    public void getProfileInfo(String profileId, final responseCallbackInterface callbackInterface) {
        String url = baseURL + "u/" + profileId;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("getProfileInfo", response);
                        try {
                            callbackInterface.callbackOnSuccess(new User(new JSONObject(response)));
                        }
                        catch(Exception e) {
                            Log.e(TAG, "getProfileInfo::onResponse: "+e.toString());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callbackInterface.callbackOnFailure();
            }
        });
        requestQueue.add(stringRequest);
    }

    public void updateProfileInfo(final User user, final responseCallbackInterface callbackInterface) {
        String url = baseURL;
        Log.e("POST", user.toJson().toString());
        int method;
        if(EditProfileActivity.isNewUser) {
            method = Request.Method.POST;
            url += "u";
            EditProfileActivity.isNewUser = false;
            Log.e("new user", ""+EditProfileActivity.isNewUser);
        }
        else {
            method = Request.Method.PUT;
            url += "u/" + user.ID;
        }
        JsonObjectRequest jsonRequest = new JsonObjectRequest(method, url, user.toJson(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("update response", response.toString());
                callbackInterface.callbackOnSuccess((User)null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callbackInterface.callbackOnSuccess((User)null);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        requestQueue.add(jsonRequest);
    }

    public void getMatches(String profileId, final responseCallbackInterface callbackInterface) {
        String url = baseURL + "m/" + profileId;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("getMatches", response);
                        try {
                            JSONArray jarray = new JSONArray(response);
                            User uarray[] = new User[jarray.length()];
                            for(int i=0; i<jarray.length(); i++) {
                                uarray[i] = new User(jarray.getJSONObject(i));
                            }
                            callbackInterface.callbackOnSuccess(uarray);
                        }
                        catch(Exception e) {
                            Log.e(TAG, "getMatches::onResponse: "+e.toString());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callbackInterface.callbackOnFailure();
            }
        });
        requestQueue.add(stringRequest);
    }
}
