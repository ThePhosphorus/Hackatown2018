package com.beast.hackatown.hackatown;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Marc on 2018-01-20.
 */

public class User extends JSONObject {
    public User(String ID, String userName, String email, String bio, String[] interests, String[] knowledges, int match, int score) {
        this.ID = ID;
        UserName = userName;
        Email = email;
        Bio = bio;
        Interests = interests;
        Knowledges = knowledges;
        Match = match;
        Score = score;
    }

    public String ID, UserName, Email, Bio;
    public String[] Interests, Knowledges;
    public int Match, Score;

  public User(JSONObject user){
        try {
            ID = user.getString("ID");
            UserName = user.getString("UserName");
            if(UserName == "null") {
                UserName = "";
            }
            Email = user.getString("Email");

            if (user.getString("Interests") == "null") {
                Interests = new String[0];
            }
            else {
                JSONArray array = user.getJSONArray("Interests");
                Interests = new String[array.length()];
                for(int i=0; i<array.length(); i++) {
                    Interests[i] = array.get(i).toString();
                }
            }
            if (user.getString("Knowledges") == "null") {
                Knowledges = new String[0];
            }
            else {
                JSONArray array = user.getJSONArray("Knowledges");
                Knowledges = new String[array.length()];
                for(int i=0; i<array.length(); i++) {
                    Knowledges[i] = array.get(i).toString();
                }
            }
            Bio = user.getString("Bio");
            if(Bio == "null") {
                Bio = "";
            }
            double matchFloat = user.getDouble("Match");
            matchFloat*=100;
            Match = (int)matchFloat;

            Score = user.getInt("Score");
        }catch(Exception e){
            Log.e("User", e.toString());
        }
    }

    public JSONObject toJson() {
      JSONObject json = new JSONObject();
      try {
          JSONObject temp = new JSONObject();
          json.put("ID", ID);
          json.put("Bio", Bio);
          json.put("UserName", UserName);
            JSONArray tmp = new JSONArray();
            for(int i=0; i<Interests.length; i++) {
                tmp.put(Interests[i]);
            }
          json.put("Interests", tmp);
          tmp = new JSONArray();
          for(int i=0; i<Knowledges.length; i++) {
              tmp.put(Knowledges[i]);
          }
          json.put("Knowledges", tmp);

          //json.put(ID, temp.toString());
      }
      catch(JSONException e) {

      }

        return json;
    }

    public String toString(){

      String res = "Interests : \n \t \t";

        for (int i = 0; i < Interests.length; i++) {
            res += Interests[i];

            if(i<Interests.length-1)
                  res+=  ",";
        }

        res +=" \n\nKnowledge : \n \t \t";

        for (int i = 0; i < Knowledges.length; i++) {
            res +=Knowledges[i];

            if(i<Knowledges.length-1)
                res+=  ",";
        }
        res +=" \n";
      return res;
    }
}
