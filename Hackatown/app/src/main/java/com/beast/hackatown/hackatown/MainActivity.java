package com.beast.hackatown.hackatown;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONArray;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //authentification
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener authListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MessagingActivity.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Intent intent = new Intent(this,LoginActivity.class);
        startActivity(intent);

        //get firebase auth instance
        mAuth = FirebaseAuth.getInstance();

        //get current user
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    // user auth state is changed - user is null
                    // launch login activity
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    //finish();
                }
            }
        };



       ListView listView  = (ListView) findViewById(R.id.main_layout);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                User user =(User)parent.getItemAtPosition(position);

                Intent intent = new Intent(MainActivity.this, ViewProfileActivity.class);
                intent.putExtra("profileId", user.ID);
                intent.putExtra("match", user.Match);

                startActivity(intent);
            }
        });

    refreshMatches();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id){
            case R.id.item_edit_profile:
                Intent intent = new Intent(MainActivity.this, EditProfileActivity.class);
                intent.putExtra("profileId", mAuth.getUid());
                startActivity(intent);
                break;
            case R.id.item_logout:
                mAuth.signOut();
                break;
            case R.id.item_settings:
                Intent intent2 = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent2);
                break;
            case R.id.item_refresh_matches:
                refreshMatches();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    class ProfileButtonListener implements View.OnClickListener {
        int profileId;

        public ProfileButtonListener(int profileId) {
            this.profileId = profileId;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, ViewProfileActivity.class);
            intent.putExtra("profileId", profileId);

            startActivity(intent);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(authListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null) {
            mAuth.removeAuthStateListener(authListener);
        }
    }

    public void refreshMatches(){
        Communication communication = new Communication(this);
        communication.getMatches(mAuth.getUid(), new Communication.responseCallbackInterface() {
            @Override
            public void callbackOnSuccess(User user) {

            }

            @Override
            public void callbackOnSuccess(User[] users) {
                showMatches(users);
            }

            @Override
            public void callbackOnFailure() {
                showErrorMessage();
            }
        });
    };

    public void showMatches(User[] matches) {
       /* LinearLayout layout = (LinearLayout) findViewById(R.id.main_layout);
        layout.removeAllViews();
        for(int i=0; i<matches.length; i++) {
            Button button = new Button(this);
            button.setText(matches[i].toString());
            button.setOnClickListener(new ProfileButtonListener(i));
            layout.addView(button);
        }*/


        ArrayAdapter<User> newAdaptor = new ArrayAdapter<User>(this,
                android.R.layout.simple_list_item_1, matches);

        ListView listView = (ListView) findViewById(R.id.main_layout);
        listView.setAdapter(newAdaptor);



    }

    public void showMatch(User user, ListView layout){

        Button button = new Button(this);
        layout.addView(button);

        LinearLayout newLayout = new LinearLayout(this);
        newLayout.setOrientation(LinearLayout.VERTICAL);


    }

    public void showErrorMessage() {
      /*  ListView layout = (ListView) findViewById(R.id.main_layout);
        //layout.removeAllViews();
        TextView errorText = new TextView(this);
        errorText.setText("Unable to fetch matches");
       // layout.addView(errorText);*/
        testUser();
    }

    private void testUser(){
        String[] intest = new String[3];
        String [] knowledge = new String[3];

        for (int i = 0; i < 3; i++) {
            intest[i]="interest" + i;
            knowledge[i]="Knowledge" + i;
        }
        User user = new User("5a64613857a2d65b5cd43f6b", "Marcolino", "fuck@you.com", "I like long walks on the beach", intest, knowledge, 23, 52);
        //showMatch(user,(ListView) findViewById(R.id.main_layout));

        User[] users = new User[6];

        for (int i = 0; i < 6; i++) {
            users[i]=user;
        }

        showMatches(users);

    }

}


